package ru.malyshev.restkafka.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.malyshev.restkafka.service.SendMessageService;

@RestController
public class KafkaRestController {

    SendMessageService sendMessageService;

    public KafkaRestController(SendMessageService sendMessageService) {
        this.sendMessageService = sendMessageService;
    }

    @PostMapping("/send")
    public ResponseEntity<Response> receiveMessage(@RequestBody Message message) {
        try {
            sendMessageService.sendMessage(message.getMessage());
            Response response = new Response(true, "Message was sent successfully");
            return new ResponseEntity<>(response, HttpStatus.CREATED);
        } catch (Exception exception) {
            Response response = new Response(false, "Failed to send message: " + exception.getMessage());
            return new ResponseEntity<>(response, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    public static class Message {
        private String message;

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }

    public static class Response {
        private boolean success;
        private String message;

        public Response(boolean success, String message) {
            this.success = success;
            this.message = message;
        }

        public boolean isSuccess() {
            return success;
        }

        public void setSuccess(boolean success) {
            this.success = success;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }
    }
}



