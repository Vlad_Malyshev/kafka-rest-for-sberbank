package ru.malyshev.restkafka.model;

public record StringValue(long id, String value) {}
