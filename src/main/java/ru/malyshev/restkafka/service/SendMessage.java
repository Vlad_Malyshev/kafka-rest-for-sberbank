package ru.malyshev.restkafka.service;

public interface SendMessage {
    void sendMessage(String message) throws Exception;
}
