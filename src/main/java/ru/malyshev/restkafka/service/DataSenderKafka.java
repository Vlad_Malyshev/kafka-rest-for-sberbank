package ru.malyshev.restkafka.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.support.SendResult;
import ru.malyshev.restkafka.model.StringValue;

import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Consumer;

public class DataSenderKafka implements DataSender {
    private static final Logger log = LoggerFactory.getLogger(DataSenderKafka.class);

    private final KafkaTemplate<String, StringValue> template;

    private final Consumer<StringValue> sendAsk;

    private final String topic;

    public DataSenderKafka(
            String topic,
            KafkaTemplate<String, StringValue> template,
            Consumer<StringValue> sendAsk) {
        this.topic = topic;
        this.template = template;
        this.sendAsk = sendAsk;
    }

    @Override
    public void send(StringValue value) throws Exception {
        log.info("value:{}", value);
        CompletableFuture<SendResult<String, StringValue>> future = template.send(topic, value)
                .whenComplete(
                        (result, ex) -> {
                            if (ex == null) {
                                log.info(
                                        "message id:{} was sent, offset:{}",
                                        value.id(),
                                        result.getRecordMetadata().offset());
                                sendAsk.accept(value);
                            } else {
                                log.error("message id:{} was not sent", value.id(), ex);
                            }
                        });
        try {
            future.get();
        } catch (ExecutionException e) {
            throw new Exception(value.id() + " " + e.getCause());
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
            throw new Exception("The operation was interrupted", e);
        }
    }
}
