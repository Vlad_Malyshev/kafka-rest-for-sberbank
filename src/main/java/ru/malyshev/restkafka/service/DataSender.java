package ru.malyshev.restkafka.service;

import ru.malyshev.restkafka.model.StringValue;

public interface DataSender {
    void send(StringValue value) throws Exception;
}
