package ru.malyshev.restkafka.service;

import ru.malyshev.restkafka.model.StringValue;

import java.util.concurrent.atomic.AtomicLong;

public class SendMessageService implements SendMessage {
    private final AtomicLong nextValue = new AtomicLong(1);
    private final DataSender valueConsumer;

    public SendMessageService(DataSender dataSender) {
        this.valueConsumer = dataSender;
    }

    @Override
    public void sendMessage(String message) throws Exception{
        try {
            valueConsumer.send(makeValue(message));
        } catch (Exception exception) {
            throw exception;
        }
    }

    private StringValue makeValue(String message) {
        var id = nextValue.getAndIncrement();
        return new StringValue(id, "stVal:" + message);
    }
}
